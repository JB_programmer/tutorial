
package Programas;

import java.util.*;
import java.*;
/**
 *
 * @author JUAN
 */
public class Array12 {
public static String capitalizar(String line)
{
  return Character.toUpperCase(line.charAt(0)) + line.substring(1);
}
    public static void main(String[] args) {
        String[] nombres = new String[100];
        int[] edades = new int[100];
        int[] hermanos = new int[100];
        int n = 0, h = 0, e = 0, opcion = 0;
        int mayorEdad = 0, posicion = 0;
        String nombre, alumnos="";
        do {
            n++;
            System.out.println("Alumno " + n);
            System.out.println("¿Nombre? (*=FIN)");
            nombre = cs1.Keyboard.readString();
            if (nombre.equals("*") == false) {
                nombres[n - 1] = nombre;
                System.out.println("¿edad?");
                e = cs1.Keyboard.readInt();
                edades[n - 1] = e;
                System.out.println("¿hermanos?");
                h = cs1.Keyboard.readInt();
                hermanos[n - 1] = h;
            } else {
                System.out.println("FIN");
            }
        } while (nombre.equals("*")==false);
        do{
        System.out.println("MENU");
        System.out.println("---------------");
        System.out.println("1.visualizar datos");
        System.out.println("2.Nombre del alumno de mayor edad");
        System.out.println("3.Nombre de alumnos con mas de 2 hermanos");
        System.out.println("4.Salir");
        System.out.println("Introduce un numero del 1-4");
        opcion = cs1.Keyboard.readInt();
        while (opcion < 1 || opcion > 4) {
            System.out.println("Error, opcion no valida");
            System.out.println("Introduce un numero del 1-4");
            opcion = cs1.Keyboard.readInt();
        }
        switch (opcion) {
            case 1:
                for (int i = 0; i < n-1; i++) {
                    System.out.println("ALUMNO " + nombres[i] + "\n " + edades[i] + " años, hermano/s " + hermanos[i]);
                }
            break;
            case 2:
                for (int i = 0; i < n; i++) {
                    if (edades[i] >= mayorEdad) {
                        mayorEdad = edades[i];
                        posicion = i;
                    }
                }
                System.out.println("EL ALUMNO " + nombres[posicion].toUpperCase() + " es el de mayor edad con " + edades[posicion] + " años");
                break;
            case 3:
               
                for (int i = 0; i < n; i++) {
                    if (hermanos[i] >= 2) {
                        
                        alumnos =alumnos+"\n"+capitalizar(nombres[i]);
                    }
                }
                System.out.println("Alumnos con mas de 2 hermanos:");
                System.out.println(alumnos);
                break;
        }
        }while(opcion!=4);

    }
}
