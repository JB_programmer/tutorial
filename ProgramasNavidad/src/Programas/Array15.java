/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Programas;

import java.util.Arrays;

/**
 *
 * @author JUAN
 */
public class Array15 {
    public static String[] copiaArray(String[] original){
    String[] copia=original;
    return copia;
    }
    public static String[] copiaArray2(String[] original){
    return original;
    }
    public static int[] copiaArray(int[] array){
    int[] copia=array;
    return copia;
    }
    public static float[] copiaArray(float[] array){
    float[] copia=array;
    return copia;
    }
    public static double[] copiaArray(double[] array){
    double[] copia=array;
    return copia;
    }
    
    public static void main (String[]args){
    //declarar un Array
        String[] cadenas={"perro","tanque","frigorifico","ordenador"};
    //utilizar el metodo de copiaArray
       String[]copia= copiaArray(cadenas);
       String[]copia2=cadenas;
    //imprimir el Array copiado
        System.out.println("IMPRIMIR COPIA DEL ARRAY");
        for (int i = 0; i < copia2.length; i++) {
           System.out.println(copia[i]); 
        }
        
    //imprimir el contenido del Array en orden inverso
        System.out.println("INVERTIR EL ORDEN DEL ARRAY COPIADO");
        for (int i = (copia.length-1); i>=0; i--) {
            System.out.println(copia[i]);
        }
    //ordenar los elementos del Array Copiado
        System.out.println("IMPRIMIR ORDENADOS SUS ELEMENTOS");
        Arrays.sort(copia);
        for (int i = 0; i < copia.length; i++) {
            System.out.println(copia[i]);
        }
    }
}
