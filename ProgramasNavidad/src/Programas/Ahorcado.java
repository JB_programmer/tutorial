package Programas;

public class Ahorcado {

    public static String obtenerPalabra(int aleatorio, String[] palabra) {
        return palabra[aleatorio];
    }

    public static void main(String[] args) {

   

        String palabra[] = {"platano","naranja","fresa","manzana"};

        boolean repetir = false, finBucle = false;
        int pacertadas = 0, pfalladas = 0;
        do {
            int aleatorio = (int) (Math.random() * palabra.length) + 1;
            int aciertos = 0, fallos = 0;
            //El programa selecciona la palabra secreta de forma aleatoria entre al menos 10 palabras posibles.
            String palabraAdivinar = obtenerPalabra(aleatorio, palabra);
            //Al principio del juego el programa muestra tantos ‘-’ como letras tenga la palabra secreta 
            int longitudPalabra = palabraAdivinar.length();
            StringBuffer palabraSecreta = new StringBuffer();
            StringBuffer letrasErroneas = new StringBuffer();
            StringBuffer letrasAcertadas = new StringBuffer();
            for (int i = 0; i < longitudPalabra; i++) {
                palabraSecreta.append("-");
            }
            System.out.println("ADIVINA LA PALABRA DE " + longitudPalabra + " LETRAS");
            System.out.println("puedes cometer un maximo de 5 fallos");
            System.out.println(palabraSecreta);
            //Comenzamos a preguntar por las letras y colocarlas o no en funcion de si acierta el usuario mientras no cometa mas de 5 fallos
            while (fallos < 6 && aciertos < longitudPalabra) {
                System.out.print("Teclea una letra: ");
                char letra = cs1.Keyboard.readChar();
                //validamos si lo que teclea el usuario es o no una letra
                while (Character.isLetter(letra) == false) {
                    System.out.println("Error, introduzca una letra");
                    letra = cs1.Keyboard.readChar();
                }
                //Analizamos si la letra se encuentra en la palabra, buscando el indice que ocupa
                int index = palabraAdivinar.toString().indexOf(letra);
                //si el indice es distinto de -1 significa que la letra ocupa al menos un lugar en la palabra
                //si acierta colocamos la letra en los lugares correspondientes
                if (index != -1) {
                    while (index != -1) {
                        //introducimos la letra en la posicion de nuestro StringBuffer
                        palabraSecreta.setCharAt(index, letra);
                        aciertos++;
                        //volvemos a lanzar el indexOf para saber si nuestra letra se repite
                        index = palabraAdivinar.toString().indexOf(letra, index + 1);
                    }
                    int index3 = letrasAcertadas.toString().indexOf(letra);
                    //si el indice nos da un -1 nos indica que no la hemos dicho, portanto contamos un acierto y la añadimos a nuestro StringBuffer
                    if (index3 == -1) {
                        //añadimos la letra que no esta a un nuevo StringBuffer
                        letrasAcertadas.append(letra);
                        
                        //si el indice es distinto de -1 nos idica que ya la hemos escrito, lanzamos un mensaje de error
                    } else if (index3 != -1) {
                        System.out.println("Letra ya introducida anteriormente");
                        aciertos--;
                    }
                } //si el indice es igual a -1 significa que la letra no esta
                else if (index == -1) {
                    //lanzamos un nuevo buscador para ver si hemos repetido una letra
                    int index2 = letrasErroneas.toString().indexOf(letra);
                    //si el indice nos da un -1 nos indica que no la hemos dicho, portanto contamos un fallo y la añadimos a nuestro StringBuffer
                    if (index2 == -1) {
                        //añadimos la letra que no esta a un nuevo StringBuffer
                        letrasErroneas.append(letra);
                        fallos++;
                        //si el indice es distinto de -1 nos idica que ya la hemos escrito, lanzamos un mensaje de error
                    } else if (index2 != -1) {
                        System.out.println("Letra ya introducida anteriormente");
                    }
                }
                //mostramos al usuario el resultado que va obteniendo
                System.out.println(palabraSecreta);
                System.out.println("Letras falladas: " + letrasErroneas);
            }
            if (aciertos == longitudPalabra) {
                System.out.println("Has Acertado!");
                pacertadas++;
            } else if (fallos > 5) {
                System.out.println("Has sido Ahorcado");
                pfalladas++;
            }
            System.out.println("¿Desea volver a jugar? s/n");
            char sino = cs1.Keyboard.readChar();
            if (sino == 's') {
                repetir = true;
            } else if(sino=='n'){
                repetir = false;
            }
        } while (repetir == true);
        System.out.println("INFORME DEL JUEGO");
        System.out.println("Número de rondas " + ((pacertadas) + (pfalladas)));
        System.out.println("Palabras adivinadas " + pacertadas);
        System.out.println(pfalladas + " veces ahorcado");
    }
}
