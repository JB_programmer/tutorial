/*
 Una empresa posee 5 sucursales en diferentes provincias de Castilla y León. Escribe un programa que pregunte para cada sucursal:  
 la provincia,   
 el número de ventas efectuadas y 
 el importe de cada una de las ventas.  
 Con estos datos presentará en pantalla el importe total vendido en  cada sucursal y el importe total vendido en Castilla y León. 
 */
package Programas;

/**
 *
 * @author JUAN
 */
public class Array11 {

    public static void main(String[] args) {
        String[] sucursales = new String[5];
        double[] ventas=new double[sucursales.length];
        double suma=0,total=0;
        for (int i = 0; i < sucursales.length; i++) {
            System.out.print("SUCURSAL "+(i+1)+": ¿Provincia? ");
            String provincia=cs1.Keyboard.readString();
            sucursales[i]=provincia.toUpperCase();
            System.out.print ("Intoducir numero de ventas: ");
            int nVentas=cs1.Keyboard.readInt();
            for (int j = 0; j < nVentas; j++) {
                System.out.println("Importe venta "+(j+1)+":");
                double importe=cs1.Keyboard.readDouble();
                suma=suma+importe;
            }
            ventas[i]=suma;
            suma=0;
        }
        for (int i = 0; i < sucursales.length; i++) {
            System.out.println("Total vendido por la SUCURSAL "+(i+1)+" "+sucursales[i]+" :"+ventas[i]+"€");
            total=total+ventas[i];
        }
        System.out.println("importe total vendido por la empresa "+total+"€");
    }
}
