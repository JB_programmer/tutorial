/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Programas;

/**
 *
 * @author JUAN
 */
public class Array13 {

    public static void main(String[] args) {
    char[] soluciones={'a','b','c','d','a','b','c','d','a','b'};
    char opcion;
    int fallos=0,aciertos=0;
        for (int i = 0; i < soluciones.length; i++) {
            System.out.print("PREGUNTA "+(i+1)+". ¿Respuesta correcta? (a,b,c,d): ");
            opcion=cs1.Keyboard.readChar();
            while(opcion!='a' && opcion!='b' && opcion!='c' && opcion!='d'){
                System.out.println("Error . las unicas respuestas posibles son a,b,c,d ");
                System.out.print("PREGUNTA "+(i+1)+". ¿Respuesta correcta? (a,b,c,d): ");
            opcion=cs1.Keyboard.readChar();
            }
            if(Character.toLowerCase(opcion)!=soluciones[i]){
            fallos++;
                System.out.println("Respuesta incorrecta. La respuesta correcta es "+soluciones[i]);
            }
            else{
            aciertos++;
                System.out.println("Respuesta correcta");
            }
        }
        System.out.println("Respuestas acertadas "+aciertos);
        System.out.println("Respuestas falladas "+fallos);
    }
}
